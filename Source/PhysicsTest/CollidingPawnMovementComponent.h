// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "CollidingPawnMovementComponent.generated.h"


UCLASS()
class PHYSICSTEST_API UCollidingPawnMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()
	UCollidingPawnMovementComponent();
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

protected:
	// How fast the pawn moves
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed")
		float speed;

	// If the player is currently jumping
	bool isJumping;

	// This is used to count which frame of the jump it is currently on
	int jumpTick;

public:
	//Setters and getters
	void SetIsJumping(bool jump);
	bool GetIsJumping() { return isJumping; }
	
};
