// Fill out your copyright notice in the Description page of Project Settings.

#include "CollidingPawn.h"
#include "Components/SphereComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ConstructorHelpers.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "ParticleHelper.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "CollidingPawnMovementComponent.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"

// Sets default values
ACollidingPawn::ACollidingPawn()
{
	m_isCrouching = false;

	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_capsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("RootComponent"));
	RootComponent = m_capsuleComponent;
	m_capsuleComponent->InitCapsuleSize(25.f, 40.f);
	m_capsuleComponent->SetCollisionProfileName(TEXT("Pawn"));

	// Create and position a mesh component so we can see where our sphere is
	UStaticMeshComponent* PawnStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PawnStaticMesh"));
	PawnStaticMesh->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("/Game/Meshes/Shape_Sphere.Shape_Sphere"));

	if (MeshAsset.Succeeded())
	{
		PawnStaticMesh->SetStaticMesh(MeshAsset.Object);
		PawnStaticMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));
		PawnStaticMesh->SetWorldScale3D(FVector(0.8f));
	}
	
	// Initialise spring arm and attach it to the root component
	 m_springArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	m_springArm->SetupAttachment(RootComponent);
	m_springArm->RelativeRotation = FRotator(0.f, 0.f, 0.f);	// No rotation
	m_springArm->TargetArmLength = 300.f;		// Set length of the spring arm
	m_springArm->bEnableCameraLag = false;	// No camera lag

	// Create a camera and attach to our spring arm
	 m_camera = CreateDefaultSubobject<UCameraComponent>(TEXT("ActualCamera"));
	m_camera->SetupAttachment(m_springArm, USpringArmComponent::SocketName);
	// Offset the camera so it is behind and to the right of the pawn
	m_camera->RelativeLocation = FVector(0.f, 130.f, 80.f);

	// Take control of the default player
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	// Create an instance of our movement component, and tell it to update our root component.
	m_movementComponent = CreateDefaultSubobject<UCollidingPawnMovementComponent>(TEXT("CustomMovementComponent"));
	m_movementComponent->UpdatedComponent = RootComponent;
}

// Called when the game starts or when spawned
void ACollidingPawn::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACollidingPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ACollidingPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// Bind player inputs
	InputComponent->BindAxis("MoveForward", this, &ACollidingPawn::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ACollidingPawn::MoveRight);
	InputComponent->BindAxis("Turn", this, &ACollidingPawn::Turn);
	InputComponent->BindAxis("LookUp", this, &ACollidingPawn::LookUp);
	InputComponent->BindAxis("Crouch", this, &ACollidingPawn::Crouch);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ACollidingPawn::Jump);

}

UPawnMovementComponent* ACollidingPawn::GetMovementComponent() const
{
	// Return pointer to the movement component
	return m_movementComponent;
}

void ACollidingPawn::MoveForward(float AxisValue)
{
	// Check that there is a movement component and it is being updated by the root component
	if (m_movementComponent && (m_movementComponent->UpdatedComponent == RootComponent))
	{
		// Add to input vector
		m_movementComponent->AddInputVector(m_camera->GetForwardVector() * AxisValue);
	}
}

void ACollidingPawn::MoveRight(float AxisValue)
{
	// Check that there is a movement component and it is being updated by the root component
	if (m_movementComponent && (m_movementComponent->UpdatedComponent == RootComponent))
	{
		// Add to input vector
		m_movementComponent->AddInputVector(m_camera->GetRightVector() * AxisValue);
	}
}

void ACollidingPawn::Turn(float AxisValue)
{
	// Rotate the spring arm with mouse left/right input
	FRotator NewRotation = m_springArm->RelativeRotation;
	NewRotation.Yaw += AxisValue;
	m_springArm->RelativeRotation = NewRotation;

	// Rotate the pawn with mouse left/right input
	FRotator newRotation = GetActorRotation();
	newRotation.Yaw += AxisValue;
	SetActorRotation(newRotation);
}

void ACollidingPawn::LookUp(float AxisValue)
{
	// Rotate the camera with mouse up/down input
	FRotator NewRotation = m_springArm->RelativeRotation;
	NewRotation.Pitch += -AxisValue;
	m_springArm->RelativeRotation = NewRotation;

}

void ACollidingPawn::Jump()
{
	// Create hit result and start vector
	FHitResult hit;
	FVector start = GetActorLocation();
	start.Z -= 20.f;
	
	// Trace from the start vector down 2000 units.
	Trace(GetWorld(), this, start, -GetActorUpVector(), 2000, hit);

	// If player is not jumping and is within 70 units of the ground, they can jump
	if (!m_movementComponent->GetIsJumping() && (start.Z - hit.Location.Z) < 70.f)
	{
		// Jump
		m_movementComponent->SetIsJumping(true);
	}
	
}

void ACollidingPawn::Crouch(float AxisValue)
{
	// If the player is current crouch, the button is released, and there is space to stand
	if (m_isCrouching == true && AxisValue == 0 && CanStand())
	{
		// Stop crouching
		m_isCrouching = false;
		m_capsuleComponent->SetCapsuleSize(25.f, 40.f); // Reset the capsule size to normal
	}
	else if(AxisValue == 1) // Else if the button is being pressed
	{
		m_isCrouching = true;
		m_capsuleComponent->SetCapsuleSize(25.f, 20.f); // Half the capsule height
	}
}


//Trace using start point, direction, and length
bool ACollidingPawn::Trace(UWorld* world, AActor* actorToIgnore, const FVector& start, const FVector& dir, float length, FHitResult& hit, ECollisionChannel collisionChannel, bool returnPhysMat) {
	return Trace(world, actorToIgnore, start, start + dir * length, hit, collisionChannel, returnPhysMat);
}

//Trace using start point, and end point
bool ACollidingPawn::Trace(UWorld* world, AActor* actorToIgnore, const FVector& start, const FVector& end, FHitResult& hit, ECollisionChannel collisionChannel, bool returnPhysMat) {
	if (!world)
	{
		return false;
	}

	// Trace params, set the 'false' to 'true' if you want it to trace against the actual meshes instead of their collision boxes.
	FCollisionQueryParams TraceParams(FName(TEXT("VictoreCore Trace")), false, actorToIgnore);
	TraceParams.bReturnPhysicalMaterial = returnPhysMat;

	// Ignore Actors, usually the actor that is calling the trace
	TraceParams.AddIgnoredActor(actorToIgnore);

	// Re-initialize hit info, so you can call the function repeatedly and hit will always be fresh
	hit = FHitResult(ForceInit);

	// Trace!
	bool hitSomething = world->LineTraceSingleByChannel(
		hit,				// result
		start,				// start
		end,				// end
		collisionChannel,	// collision channel
		TraceParams			// trace parameters
	);

	// Draw a square at the impact point.
	if (hitSomething) DrawDebugPoint(world, hit.ImpactPoint, 10, FColor(255, 255, 0), false, 5);

	// Draw the trace line. Red if something was hit, green if nothing was hit.
	DrawDebugLine(world, start, end, (hitSomething ? FColor(255, 0, 0) : FColor(0, 255, 0)), false, 5.f, 0, 1.5);

	return hitSomething;
}

bool ACollidingPawn::CanStand()
{
	// Raycast from the top of the actor
	FVector start = GetActorLocation();
	start.Z += 20.f;

	FHitResult hit; // FHitResult to store hit results

	FVector cornerOffset = { 25.f,25.f,0.f }; // Vector to offset the start to each of the 4 top face corners

	// Raycast from each corner upwards, if there is a hit on any of these raycasts, return false
	if (Trace(GetWorld(), this, start + cornerOffset, GetActorUpVector(), 40.f, hit)) return false;
	
	cornerOffset = { 25.f,-25.f,0.f };
	if (Trace(GetWorld(), this, start + cornerOffset, GetActorUpVector(), 40.f, hit)) return false;

	cornerOffset = { -25.f,25.f,0.f };
	if (Trace(GetWorld(), this, start + cornerOffset, GetActorUpVector(), 40.f, hit)) return false;
	
	cornerOffset = { -25.f,-25.f,0.f };
	if (Trace(GetWorld(), this, start + cornerOffset, GetActorUpVector(), 40.f, hit)) return false;

	// If there is no hit from the raycasts, return true
	return true;
}
