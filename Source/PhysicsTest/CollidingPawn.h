// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "CollidingPawn.generated.h"

UCLASS()
class PHYSICSTEST_API ACollidingPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACollidingPawn();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MyBPForCPP")
		TSubclassOf<ACharacter> m_thirdPersonCharacter;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Raycasting functions
	static bool Trace(UWorld* world, AActor* actorToIgnore, const FVector& start, const FVector& dir, float length, FHitResult& hit, ECollisionChannel CollisionChannel = ECC_Pawn, bool ReturnPhysMat = false);
	static bool Trace(UWorld* world, AActor* actorToIgnore, const FVector& start, const FVector& end, FHitResult& hit, ECollisionChannel CollisionChannel = ECC_Pawn, bool ReturnPhysMat = false);

	// Bool for switching between crouching and standing
	bool m_isCrouching;

	// Function that raycasts upwards to and returns true if there is enough space to stand up
	bool CanStand();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Movement component handles the movement of the pawn
	class UCollidingPawnMovementComponent* m_movementComponent;

	// The capsule component is used for collisions
	class UCapsuleComponent * m_capsuleComponent;

	// Camera component is the player's perspective when they possess the pawn
	class UCameraComponent* m_camera;

	// Spring arm attaches the camera to the pawn for smooth movement
	class USpringArmComponent* m_springArm;

	// Returns the movement component
	virtual UPawnMovementComponent* GetMovementComponent() const override;

	// Functions to handle player input
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void Turn(float AxisValue);
	void LookUp(float AxisValue);
	void Jump();
	void Crouch(float AxisValue);
};
