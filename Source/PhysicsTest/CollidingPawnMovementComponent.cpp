// Fill out your copyright notice in the Description page of Project Settings.

#include "CollidingPawnMovementComponent.h"

UCollidingPawnMovementComponent::UCollidingPawnMovementComponent()
{
	// Initialise default values
	speed = 500.f;
	isJumping = false;
	jumpTick = 0;
}


void UCollidingPawnMovementComponent::SetIsJumping(bool jump)
{
	// Set isJumping
	isJumping = jump;
}


void UCollidingPawnMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	

	// Make sure that everything is still valid, and that we are allowed to move.
	if (!PawnOwner || !UpdatedComponent || ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	// Get (and then clear) the movement vector that we set in ACollidingPawn::Tick
	// Normalises the movement vector and multiplies by delta time and speed
	FVector DesiredMovementThisFrame = ConsumeInputVector().GetClampedToMaxSize(1.f)* DeltaTime * speed ;

	DesiredMovementThisFrame.Z = 0.f; // Reset all Z movement to 0

	// If isJumping is true
	if (isJumping)
	{
		jumpTick++;
		// If jumpTick is bigger than 15, increment Z by 3, if it is lower than 15, increment Z by 3 + jumpTick
		// This is so that the beginning of the jump is faster
		DesiredMovementThisFrame.Z += (jumpTick > 15) ? 3 : 3 + jumpTick; 
		if (jumpTick > 25) // Max jumpTicks is 26
		{
			// Finished jumping up
			jumpTick = 0;
			isJumping = false;
		}
	}
	else
	{
		// If not jumping, normal gravity is applied
		DesiredMovementThisFrame.Z -= 3.f;
	}

	// If movement is not nearly zero
	if (!DesiredMovementThisFrame.IsNearlyZero())
	{
		FHitResult Hit;
		// Move
		SafeMoveUpdatedComponent(DesiredMovementThisFrame, UpdatedComponent->GetComponentRotation(), true, Hit);

		// If we bumped into something, try to slide along it
		if (Hit.IsValidBlockingHit())
		{
			SlideAlongSurface(DesiredMovementThisFrame, 1.f - Hit.Time, Hit.Normal, Hit);
		}	
	}

	
}
